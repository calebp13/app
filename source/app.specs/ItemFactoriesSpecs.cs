﻿using System;
using System.Collections.Generic;
using Machine.Specifications;
using app.utility.containers;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(ItemFactories))]
  public class ItemFactoriesSpecs
  {
    public abstract class concern : Observes<IFindFactoriesThatCreateItems,
                                      ItemFactories>
    {
    }

    public class when_finding_the_factory_that_can_create_an_item : concern
    {
      public class and_it_has_it
      {
        Establish c = () =>
        {
          the_factory = fake.an<ICreateOneDependency>();
          var factories = new Dictionary<Type, ICreateOneDependency>
          {
            {typeof(int), the_factory}
          };
          depends.on<IDictionary<Type,ICreateOneDependency>>(factories);
        };

        Because b = () =>
          result = sut.get_the_factory_that_can_create(typeof(int));

        It should_return_the_factory_that_can_create_the_item = () =>
          result.ShouldEqual(the_factory);

        static ICreateOneDependency result;
        static ICreateOneDependency the_factory;
      }
    }
  }
}