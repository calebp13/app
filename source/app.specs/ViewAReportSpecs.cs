﻿using Machine.Specifications;
using app.web.application.catalogbrowsing;
using app.web.core;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  public class ViewAReportSpecs
  {
    public abstract class concern : Observes<ISupportAFeature, ViewAReport<AReport>>
    {
    }

    public class when_viewing_a_report : concern
    {
      Establish c = () =>
      {
        display = depends.on<IDisplayInformation>();
        request = fake.an<IContainRequestInformation>();
        the_report = new AReport();
        depends.on<FetchReportUsing_Behaviour<AReport>>(x => the_report);
      };

      Because b = () =>
        sut.run(request);

      It should_display_the_report_data = () =>
        display.received(x => x.display(the_report));

      static IDisplayInformation display;
      static IContainRequestInformation request;
      static AReport the_report;
    }

    public class AReport
    {
    }
  }
}