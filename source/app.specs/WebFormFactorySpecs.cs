﻿using System.Web;
using Machine.Specifications;
using app.web.core.aspet;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(WebFormFactory))]
  public class WebFormFactorySpecs
  {
    public abstract class concern : Observes<ICreateViewsForReports,
                                      WebFormFactory>
    {
    }

    public class when_creating_the_view_to_display_a_report : concern
    {
      Establish c = () =>
      {
        page_path_registry = depends.on<IFindPathsToAspxs>();
        the_view_that_can_display_the_report = fake.an<IDisplayA<TheItem>>();
        page_path_registry.setup(x => x.get_the_path_to_the_logical_view_for<TheItem>()).Return("blah.aspx");

        depends.on<CreatePage_Behaviour>((path, type) =>
        {
          path.ShouldEqual("blah.aspx");
          type.ShouldEqual(typeof(IDisplayA<TheItem>));
          return the_view_that_can_display_the_report;
        });
        item = new TheItem();
      };

      Because b = () =>
        result = sut.create_view_that_can_display(item);


      It should_have_populated_the_view_with_its_data = () =>
        the_view_that_can_display_the_report.report.ShouldEqual(item);
        
      It should_return_the_view_that_can_display_the_report = () =>
        result.ShouldEqual(the_view_that_can_display_the_report);
        

      static IFindPathsToAspxs page_path_registry;
      static TheItem item;
      static IHttpHandler result;
      static IDisplayA<TheItem> the_view_that_can_display_the_report;
    }

    public class TheItem
    {
    }
  }
}