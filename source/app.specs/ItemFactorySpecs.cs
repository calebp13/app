﻿using System.Data;
using System.Reflection;
using Machine.Specifications;
using app.specs.utility;
using app.utility.containers;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(ItemFactory))]
  public class ItemFactorySpecs
  {
    public abstract class concern : Observes<ICreateOneDependency,
                                      ItemFactory>
    {
    }

    public class when_creating_an_item : concern
    {
      Establish c = () =>
      {
        depends.on(typeof(ItemWithLotsOfDependencies));
        container = depends.on<IFetchDependencies>();
        ctor_picker = depends.on<IPickTheCtorOnAType>();

        the_greediest = ObjectFactory.expressions.to_target<ItemWithLotsOfDependencies>()
          .get_ctor_pointed_at_by(() => new ItemWithLotsOfDependencies(null, null, null));

        the_connection = fake.an<IDbConnection>();
        the_command = fake.an<IDbCommand>();
        the_other_item = new AnotherItem();

        ctor_picker.setup(x => x.get_the_ctor_on(typeof(ItemWithLotsOfDependencies)))
          .Return(the_greediest);

        container.setup(x => x.an(typeof(IDbConnection))).Return(the_connection);
        container.setup(x => x.an(typeof(IDbCommand))).Return(the_command);
        container.setup(x => x.an(typeof(AnotherItem))).Return(the_other_item);


      };
       
      Because b = () =>
        result = sut.create();

      It should_return_the_item_with_all_of_its_dependencies_filled = () =>
      {
        var item = result.ShouldBeAn<ItemWithLotsOfDependencies>();
        item.connection.ShouldEqual(the_connection);
        item.command.ShouldEqual(the_command);
        item.other.ShouldEqual(the_other_item);
      };

      static object result;
      static IDbConnection the_connection;
      static IDbCommand the_command;
      static AnotherItem the_other_item;
      static IPickTheCtorOnAType ctor_picker;
      static ConstructorInfo the_greediest;
      static IFetchDependencies container;
    }

    public class ItemWithLotsOfDependencies
    {
      public IDbConnection connection;
      public IDbCommand command;
      public AnotherItem other;

      public ItemWithLotsOfDependencies(IDbConnection connection, IDbCommand command, AnotherItem other)
      {
        this.connection = connection;
        this.command = command;
        this.other = other;
      }

      public ItemWithLotsOfDependencies(IDbConnection connection, IDbCommand command)
      {
        this.connection = connection;
        this.command = command;
      }
    }

    public class AnotherItem
    {
    }
  }
}