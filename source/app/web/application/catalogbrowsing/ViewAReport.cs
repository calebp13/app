using app.web.core;

namespace app.web.application.catalogbrowsing
{
  public class ViewAReport<ReportModel> : ISupportAFeature
  {
    IDisplayInformation display_engine;
    FetchReportUsing_Behaviour<ReportModel> query;

    public ViewAReport(IDisplayInformation display_engine, FetchReportUsing_Behaviour<ReportModel> query)
    {
      this.display_engine = display_engine;
      this.query = query;
    }

    public void run(IContainRequestInformation request)
    {
      display_engine.display(query(request));
    }
  }
}