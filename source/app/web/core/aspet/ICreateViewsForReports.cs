﻿using System.Web;

namespace app.web.core.aspet
{
  public interface ICreateViewsForReports
  {
    IHttpHandler create_view_that_can_display<ReportModel>(ReportModel report);
  }
}