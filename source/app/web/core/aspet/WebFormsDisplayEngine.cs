﻿namespace app.web.core.aspet
{
  public class WebFormsDisplayEngine : IDisplayInformation
  {
    ICreateViewsForReports view_factory;
    GetTheCurrentRequest_Behaviour current_request;

    public WebFormsDisplayEngine(ICreateViewsForReports view_factory, GetTheCurrentRequest_Behaviour current_request)
    {
      this.view_factory = view_factory;
      this.current_request = current_request;
    }


    public void display<ReportModel>(ReportModel model)
    {
      var view = view_factory.create_view_that_can_display(model);
      view.ProcessRequest(current_request());
    }
  }
}