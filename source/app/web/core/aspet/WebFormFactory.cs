﻿using System.Web;

namespace app.web.core.aspet
{
  public class WebFormFactory : ICreateViewsForReports
  {
    IFindPathsToAspxs path_registry;
    CreatePage_Behaviour real_factory;

    public WebFormFactory(IFindPathsToAspxs path_registry, CreatePage_Behaviour real_factory)
    {
      this.path_registry = path_registry;
      this.real_factory = real_factory;
    }

    public IHttpHandler create_view_that_can_display<ReportModel>(ReportModel report)
    {
      var view =
        (IDisplayA<ReportModel>) real_factory(path_registry.get_the_path_to_the_logical_view_for<ReportModel>(),
                                              typeof(IDisplayA<ReportModel>));
      view.report = report;
      return view;
    }
  }
}