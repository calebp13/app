namespace app.web.core
{
  public interface IFetchAReport<out ReportModel>
  {
    ReportModel fetch_using(IContainRequestInformation request);
  }
}