﻿namespace app.web.core
{
  public interface IProcessRequests
  {
    void process(IContainRequestInformation request);
  }

  public class FrontController : IProcessRequests
  {
    IFindCommands command_registry;

    public FrontController(IFindCommands command_registry)
    {
      this.command_registry = command_registry;
    }

    public void process(IContainRequestInformation request)
    {
      command_registry.get_the_command_that_can_handle(request).run(request);
    }
  }
}