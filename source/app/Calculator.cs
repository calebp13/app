﻿using System;
using System.Data;
using System.Security;
using System.Threading;

namespace app
{
  public interface ICalculate
  {
    int add(int i, int i1);
    void shut_off();
  }

  public class Calculator : ICalculate
  {
    IDbConnection connection ;

    public Calculator(IDbConnection connection)
    {
      this.connection = connection;
    }

    public int add(int i, int i1)
    {
      if (i < 0 || i1 < 0)
        throw new ArgumentException();

      using (connection)
      using (var command = connection.CreateCommand())
      {
        connection.Open();
        command.ExecuteNonQuery();
      }

      return i + i1;
    }

    public void shut_off()
    {
      if (Thread.CurrentPrincipal.IsInRole("GorillaCoders")) return;

		  throw new SecurityException();

    }
  }
}