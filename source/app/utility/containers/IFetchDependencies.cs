﻿using System;

namespace app.utility.containers
{
  public interface IFetchDependencies
  {
    Dependency an<Dependency>();
    object an(Type dependency);
  }

  public class DependencyFetcher : IFetchDependencies
  {
    IFindFactoriesThatCreateItems factory_finder;
    ItemCreationFailure_Behaviour on_creation_failure;

    public DependencyFetcher(IFindFactoriesThatCreateItems factory_finder,
                             ItemCreationFailure_Behaviour on_creation_failure)
    {
      this.factory_finder = factory_finder;
      this.on_creation_failure = on_creation_failure;
    }

    public Dependency an<Dependency>()
    {
      return (Dependency) an(typeof(Dependency));
    }

    public object an(Type dependency)
    {
      var factory = factory_finder.get_the_factory_that_can_create(dependency);
      try
      {
        return factory.create();
      }
      catch (Exception e)
      {
        throw on_creation_failure(dependency, e);
      }
    }
  }
}