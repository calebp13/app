﻿namespace app.utility.containers
{
  public interface ICreateOneDependency
  {
    object create();
  }
}